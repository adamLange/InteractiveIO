This is a OpenModelica Model to create an interactive simulation using socket communication and modelica functions.

For this we have, Input Interface and Output Interface blocks,which can be connected in future to the anymodels for reusability.

Current status: Once simulated,it prompts with a userInput,and the aim is to get back the reply to the same socket.

To run the exercise, first run the script init.sh:

    $./init.sh

Running init.sh will substitute hard coded paths in where necessary
and compile socketClient.c into a shared library that will be linked
to the modelica model.

Next, start the python server.  The server waits on a unix socket 
to recieve the input values.  The model mainpulates them and sends back
the result. Start the server with this command:

    $python server.py -m;rm rpcSocket

Start the Open Modelica Editor and load the package IPCExample by
navigating to CPSModel/package.mo.

**Once model is simulated,you can see the prompt for user input.
Input a Real value **


