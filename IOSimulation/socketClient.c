#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <jansson.h>

#define LOG_PATH "_projectDir_/log"

typedef struct {
	//struct sockaddr_un* address;
	//int id;
        char* sockName;
} SocketConnection;

/*
 typedef struct {
 int requestType;
 //value;
 } GetRequest;


 typedef struct {
 int
 } ;
 */

void logMessage(char* logMessage, char* type) {

	FILE *fid;
	if ((fid = fopen(LOG_PATH, "a")) <= 0) {
		perror("open log file");
		exit(1);
	}

	fprintf(fid, logMessage, type);

	fclose(fid);
}

void logMessageDouble(char* logMessage, double type) {

	FILE *fid;
	if ((fid = fopen(LOG_PATH, "a")) <= 0) {
		perror("open log file");
		exit(1);
	}

	fprintf(fid, logMessage, type);

	fclose(fid);
}

void logMessageText(char* logMessage) {

	FILE *fid;
	if ((fid = fopen(LOG_PATH, "a")) <= 0) {
		perror("open log file");
		exit(1);
	}

	fprintf(fid, logMessage);

	fclose(fid);
}


void* initSocketConnection(const char* socketName) {

	logMessageText("initSocketConnection... \n");

	SocketConnection *con = malloc(sizeof(SocketConnection));
        con->sockName = malloc(sizeof(char)*strlen(socketName)+1);
        strcpy(con->sockName, socketName);

        /*
	con->address = malloc(sizeof(struct sockaddr_un));
	int len;

	if ((con->id = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	logMessage("opened socket id %i\n", con->id);

	con->address->sun_family = AF_UNIX;

	strcpy(con->address->sun_path, socketName);
	len = strlen(con->address->sun_path) + sizeof(con->address->sun_family);

	if (connect(con->id, (struct sockaddr *) con->address, len) == -1) {
		perror("connect");
		exit(1);
	}
        */

	return (void*) con;

}


void closeSocketConnection(void* object) {

	logMessageText("closeSocketConnection... \n");

	SocketConnection *con = (SocketConnection*) object;
	//close(con->id);
	//free(con->address);
        free(con->sockName);
	free(con);
}

void sendSetRequest(void* object, char* varName, double u) {

	logMessageText("sendSetRequest begin\n");

	SocketConnection *con = (SocketConnection*) object;

	struct sockaddr_un* address;
        int id;

	address = malloc(sizeof(struct sockaddr_un));
	int len;

	if ((id = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	address->sun_family = AF_UNIX;

	strcpy(address->sun_path, con->sockName);
	len = strlen(address->sun_path) + sizeof(address->sun_family);

	if (connect(id, (struct sockaddr *) address, len) == -1) {
		perror("connect");
		exit(1);
	}

	// this function will send a request to the server to set varName = u in the server.

	// (The server will have a persistant variable named varName, or if varName doesn't exist,
	// it will be created.)
	//SocketConnection *con = (SocketConnection*) object;

	// Here, we send a message to the server using the JSON-RPC protocall
	// https://en.wikipedia.org/wiki/JSON-RPC

	char* jsonRequest = NULL;

	json_t *rootReq = json_object();
	json_t *json_arr = json_object();

	json_t *rootReply;
	json_error_t error;

	char reply[256];
	int n;

	char* parsedReply;

	// We need to make a string that will be the JSON-RPC request:

	// '{"jsonrpc": "2.0", "method": "set", "params": {"varName": "variable1", "value": %f } "id": %i}'
	// Need to substitute %f for u
	// Need to substitute %i for a unique identifier for the request (JSON-RPC request id)
	// The string could be built by hand or libjansson could be used: http://www.digip.org/jansson/
	json_object_set_new( rootReq, "jsonrpc", json_integer( 2 ) );
	json_object_set_new( rootReq, "method", json_string("set") );
	json_object_set_new( rootReq, "params", json_arr );
	json_object_set_new( rootReq, "id", json_integer(2) );

	json_object_set_new( json_arr, "varName", json_string(varName) );
	json_object_set_new( json_arr, "value", json_real(u) );

	jsonRequest = json_dumps(rootReq, 0);

	int lenJsonString = strlen(jsonRequest);

	logMessage(jsonRequest, "%s\n");
	logMessageText("\nSending request to server \n");

	// Send the json-rpc request string
	if (send(id, jsonRequest, sizeof(char) * lenJsonString, 0) <= 0) {
		perror("sending with con.id = %i\n");
		exit(1);
	}

	json_decref(rootReq);

	// Recieve the jsonReply
	logMessageText("Waiting for reply \n");
	bzero(reply, 256);

	// n = recv()
	if ((n = recv(id, reply, 256, 0)) <= 0) {
		if (n < 0)
			perror("recv");
		else
			perror("Server closed connection\n");
		exit(1);
	}

	// If varName was successfully set to u, the server will send back a reply that says ok.
	// The reply will be a json string something like this:
	// {"jsonrpc": "2.0", "result": "OK!", "id": 3}
	logMessage(reply, "%s\n");
	logMessageText("\n");

	// The json string can be parsed using libjansson
	// If the server failed, throw an error in here.
	// The message that the server sends will be a JSON string
	rootReply = json_loads( reply, 0, &error );

	if ( !rootReply ) {
	    fprintf( stderr, "error: on line %d: %s\n", error.line, error.text );
	    exit(0);
	}

	json_t * parsed = json_object_get(rootReply, "result");

	if (json_typeof(parsed) == JSON_STRING) {

		parsedReply = json_string_value(parsed);

		logMessageText("Parsed Reply : ");
		logMessage(parsedReply, "%s\n\n");

		if(strcmp(parsedReply, "OK!") == 0){
			logMessageText("\nValue has been set\n\n");
			logMessageText(parsedReply);
		} else {
			logMessageText("\nFailed to set value\n\n");
			exit(0);
		}
	} else {
		logMessageText("Parse error\n\n");
		exit(0);
	}

	logMessageText("sendSetRequest end\n");

	json_decref(rootReply);

        close(id);
        free(address);
}

double sendGetRequest(void* object, char* varName) {
	logMessageText("\nsendGetRequest begin\n");

	// send a get request to the server
	SocketConnection *con = (SocketConnection*) object;

	struct sockaddr_un* address;
        int id;

	address = malloc(sizeof(struct sockaddr_un));
	int len;

	if ((id = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	address->sun_family = AF_UNIX;

	strcpy(address->sun_path, con->sockName);
	len = strlen(address->sun_path) + sizeof(address->sun_family);

	if (connect(id, (struct sockaddr *) address, len) == -1) {
		perror("connect");
		exit(1);
	}

	char* jsonRequest = NULL;

	json_t *rootReq = json_object();
	json_t *json_arr = json_object();

	json_t *rootReply;
	json_error_t error;

	char reply[256];
	int n;
	double replyToModel;

	//-->{"jsonrpc": "2.0", "method": "get", "params": {"varName": %s} "id": %i}
	json_object_set_new( rootReq, "jsonrpc", json_integer( 2 ) );
	json_object_set_new( rootReq, "method", json_string("get") );
	json_object_set_new( rootReq, "params", json_arr );
	json_object_set_new( rootReq, "id", json_integer(2) );

	json_object_set_new( json_arr, "varName", json_string(varName) );

	jsonRequest = json_dumps(rootReq, 0);

	int lenJsonString = strlen(jsonRequest);

	logMessage(jsonRequest, "%s\n");
	logMessageText("\nSending request to server \n");

	// Send the json-rpc request string
	if (send(id, jsonRequest, sizeof(char) * lenJsonString, 0) <= 0) {
		perror("sending with con.id = %i\n");
		exit(1);
	}

	json_decref(rootReq);

	logMessageText("Waiting for reply \n");
	bzero(reply, 256);

	if ((n = recv(id, reply, 256, 0)) <= 0) {
		if (n < 0)
			perror("recv");
		else
			perror("Server closed connection\n");
		exit(1);
	}

        close(id);
        free(address);

	logMessage(reply, "%s\n");
	logMessageText("\n");

	// parse the result with jansson
	rootReply = json_loads( reply, 0, &error );
	if ( !rootReply ) {
	    fprintf( stderr, "error: on line %d: %s\n", error.line, error.text );
	    return 1;
	}

	json_t * parsed = json_object_get(rootReply, "result");

	if (json_typeof(parsed) == JSON_REAL)
		replyToModel = json_real_value(parsed);
	else {
		logMessageText("Parse error\n\n");
		exit(0);
	}

	json_decref(rootReply);

	logMessageDouble("Parsed result : %lf\n", replyToModel);
	logMessageText("sendGetRequest exit\n\n");

	return replyToModel;
}

void sendSetArrayRequest(void* object, char* varName, double* u, int arrayLength) {

	//logMessageText("sendSetRequest begin\n");

	SocketConnection *con = (SocketConnection*) object;

	struct sockaddr_un* address;
        int id;

	address = malloc(sizeof(struct sockaddr_un));
	int len;

	if ((id = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	address->sun_family = AF_UNIX;

	strcpy(address->sun_path, con->sockName);
	len = strlen(address->sun_path) + sizeof(address->sun_family);

	if (connect(id, (struct sockaddr *) address, len) == -1) {
		perror("connect");
		exit(1);
	}

	char* jsonRequest = NULL;

	json_t *rootReq = json_object();
	json_t *json_arr = json_object();

	json_t *rootReply;
	json_error_t error;

	char reply[256];
	int n;

	char* parsedReply;

	json_object_set_new( rootReq, "jsonrpc", json_integer( 2 ) );
	json_object_set_new( rootReq, "method", json_string("set") );
	json_object_set_new( rootReq, "params", json_arr );
	json_object_set_new( rootReq, "id", json_integer(2) );

	json_object_set_new( json_arr, "varName", json_string(varName) );

        json_t *array = json_array();
        for (int i = 0; i<arrayLength; i++) {
          json_array_append_new(array, json_real(u[i]) );
        }

        json_object_set_new( json_arr, "value", array);

	jsonRequest = json_dumps(rootReq, 0);

	int lenJsonString = strlen(jsonRequest);

	//logMessage(jsonRequest, "%s\n");
	//logMessageText("\nSending request to server \n");

	if (send(id, jsonRequest, sizeof(char) * lenJsonString, 0) <= 0) {
		perror("sending with con.id = %i\n");
		exit(1);
	}

	json_decref(rootReq);

	// Recieve the jsonReply
	//logMessageText("Waiting for reply \n");
	bzero(reply, 256);

	// n = recv()
	if ((n = recv(id, reply, 256, 0)) <= 0) {
		if (n < 0)
			perror("recv");
		else
			perror("Server closed connection\n");
		exit(1);
	}

	// If varName was successfully set to u, the server will send back a reply that says ok.
	// The reply will be a json string something like this:
	// {"jsonrpc": "2.0", "result": "OK!", "id": 3}
	//logMessage(reply, "%s\n");
	//logMessageText("\n");

	// The json string can be parsed using libjansson
	// If the server failed, throw an error in here.
	// The message that the server sends will be a JSON string
	rootReply = json_loads( reply, 0, &error );

	if ( !rootReply ) {
	    fprintf( stderr, "error: on line %d: %s\n", error.line, error.text );
	    exit(0);
	}

	json_t * parsed = json_object_get(rootReply, "result");

	if (json_typeof(parsed) == JSON_STRING) {

		parsedReply = json_string_value(parsed);

		logMessageText("Parsed Reply : ");
		logMessage(parsedReply, "%s\n\n");

		if(strcmp(parsedReply, "OK!") == 0){
			logMessageText("\nValue has been set\n\n");
			logMessageText(parsedReply);
		} else {
			logMessageText("\nFailed to set value\n\n");
			exit(0);
		}
	} else {
		logMessageText("Parse error\n\n");
		exit(0);
	}

	logMessageText("sendSetRequest end\n");

	json_decref(rootReply);

        close(id);
        free(address);
}
