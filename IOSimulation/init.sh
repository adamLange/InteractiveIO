#substitute in some the current directory for some hardcoded paths
projdir=$pwd
sed -i -e 's|_projectDir_|'$(pwd)'|g' socketClient.c CPSModel/ConnectionObjects.mo CPSModel/Models.mo

#compilation of the external library
clang -c -fPIC socketClient.c -o socketClient.o
mkdir -p CPSModel/ExternalLibraries
clang -shared socketClient.o -o CPSModel/ExternalLibraries/libsocketClient.so
rm socketClient.o
