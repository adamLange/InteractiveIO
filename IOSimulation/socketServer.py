import json
import socket
import argparse
import os
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("-m", "--multiConn", action="store_true",
                    help="don't quit after first connection closes")

args = parser.parse_args()

s1 = socket.socket(socket.AF_UNIX)  

s1.bind('rpcSocket')
s1.listen(5)

#tells the socket library that we want it to queue up as many as 5 connect requests (the normal max) before refusing outside connections. If the rest of the code is written properly, that should be plenty. 

acceptFlag = True

variables = {}

while acceptFlag:

  print "Waiting for connection\n"

  s2,s2info = s1.accept()
  okay = True

  while okay:
     try:
      
      print "Waiting for request \n"

      requestString = s2.recv(1024)
      print "recieved : " + str(requestString) + "\n"

      request = json.loads(requestString)
      requestID = request["id"]
      
      if (request['method'] == 'set') :
        print "SET method called \n"

        varName = request['params']['varName']
        varValue = request['params']['value']
        variables[varName] = varValue

        reply = '{"jsonrpc": "2.0", "result": "OK!", "id":' + str(requestID) + '}'

      elif (request['method'] == 'get') :
        print "GET method called \n"
        
        varName = request['params']['varName']
       
        try :
          reply = '{"jsonrpc": "2.0", "result":' + str(variables[varName]) + ', "id":' + str(requestID) + '}'
        except KeyError:
	  reply = '{"jsonrpc": "2.0", "result": "NOTOK", "id":' + str(requestID) + '}'

      elif (request['method'] == 'getKeys'):
          reply = {}
          reply['jsonrpc'] = "2.0"
          reply['id'] = requestID
          reply['result'] = variables.keys()
          reply = json.dumps(reply)

      elif (request['method'] == 'getAll'):
          reply = {}
          reply['jsonrpc'] = "2.0"
          reply['id'] = requestID
          reply['result'] = variables
          reply = json.dumps(reply)
        
      
      print "Reply : " + str(reply) + "\n"
      s2.send(str(reply).encode())
     
      # closing the connection
      #okay = False
      s2.close()

     except:
      okay = False
      s2.close()

  acceptFlag = args.multiConn


s1.close()
s2.close()
os.unlink('rpcSocket')
