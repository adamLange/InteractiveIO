# The user commands this client program to send set and get 
# requests to socketServer.py
#!/usr/bin/python           # This is client.py file

import socket
import json

s = socket.socket(socket.AF_UNIX)

s.connect("rpcSocket")

setReq = '{"jsonrpc": "2.0", "method": "set", "params": {"varName": "x", "value" : "5.00" }, "id" : "1"}'

print "Request : " + setReq + "\n"

s.sendall(setReq)

resposne = s.recv(1024)

s.close()
print "Resposne : " + resposne + "\n"
