# The user commands this client program to send set and get 
# requests to socketServer.py
#!/usr/bin/python           # This is client.py file

import socket
import json

class client():

  def __init__(self,sockName):
    self.sockName = sockName
    self.id_num = 0

  def set(self,var,value):
    self.id_num += 1
    d = {"jsonrpc":"2.0","method":"set","params":{"varName":var,"value":value},"id":self.id_num}
    s = socket.socket(socket.AF_UNIX)
    s.connect(self.sockName)
    s.sendall(json.dumps(d))
    response = s.recv(1024)
    s.close()
    dr = json.loads(response)
    return dr

  def get(self,var):
    self.id_num += 1
    d = {"jsonrpc":"2.0","method":"get","params":{"varName":var},"id":self.id_num}
    s = socket.socket(socket.AF_UNIX)
    s.connect(self.sockName)
    s.sendall(json.dumps(d))
    response = s.recv(1024)
    s.close()
    dr = json.loads(response)
    return dr

  def getKeys(self):
    self.id_num += 1
    d = {"jsonrpc":"2.0","method":"getKeys","id":self.id_num}
    s = socket.socket(socket.AF_UNIX)
    s.connect(self.sockName)
    s.sendall(json.dumps(d))
    response = s.recv(1024)
    s.close()
    dr = json.loads(response)['result']
    return dr


  def getAll(self):
    self.id_num += 1
    d = {"jsonrpc":"2.0","method":"getAll","id":self.id_num}
    s = socket.socket(socket.AF_UNIX)
    s.connect(self.sockName)
    s.sendall(json.dumps(d))
    response = s.recv(1024)
    s.close()
    dr = json.loads(response)['result']
    return dr
