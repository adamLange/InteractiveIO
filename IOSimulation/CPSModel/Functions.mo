within CPSModel;
package Functions

    function set
      input CPSModel.ConnectionObjects.Socket con;
      input String varName;
      input Real value;

      external sendSetRequest(con, varName, value) annotation(
        Library = {"socketClient","jansson"},
        LibraryDirectory = "modelica://CPSModel/ExternalLibraries");
    end set;


    function get
      input CPSModel.ConnectionObjects.Socket con;
      input String varName;
      output Real userInput;

      external "C" userInput= sendGetRequest(con,varName) annotation(
        Library = {"socketClient","jansson"},
        LibraryDirectory = "modelica://CPSModel/ExternalLibraries");
    end get;

    function setArray
      input CPSModel.ConnectionObjects.Socket con;
      input String varName;
      input Real[len] value;
      input Integer len;

      external sendSetArrayRequest(con, varName, value, len) annotation(
        Library = {"socketClient","jansson"},
        LibraryDirectory = "modelica://CPSModel/ExternalLibraries");
    end setArray;

end Functions;
