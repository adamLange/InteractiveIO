within CPSModel;
package ConnectionObjects

  class Socket extends ExternalObject;

    function constructor
      input String socName = "_projectDir_/rpcSocket";
      output Socket connection;
      external "C" connection = initSocketConnection(socName)
        annotation(Library="socketClient",
          LibraryDirectory="modelica://CPSModel/ExternalLibraries");
    end constructor;


    function destructor "close connection"
      input Socket connection;
      external "C" closeSocketConnection(connection)
        annotation(Library="socketClient",
          LibraryDirectory="modelica://CPSModel/ExternalLibraries");
    end destructor;


  end Socket;

end ConnectionObjects;
