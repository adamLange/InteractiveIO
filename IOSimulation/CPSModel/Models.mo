within CPSModel;

package Models

block InputInterfaceBlock
  parameter Real dt_sample = 0.1;
  parameter String variableName;
  parameter Real y_init;

  CPSModel.ConnectionObjects.Socket con = CPSModel.ConnectionObjects.Socket("_projectDir_/rpcSocket");

  Modelica.Blocks.Interfaces.RealOutput y annotation(
    Placement(visible = true, transformation(origin = {-14, 8}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {106, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
initial equation
  y = y_init;
equation
    when initial() then
      CPSModel.Functions.set(con,variableName,y_init);
    end when;
    when sample(0,dt_sample) then
      //print("Waiting for get.\n");
      y = CPSModel.Functions.get(con, variableName);
      //print("Message user entered : " + String(y) + "\n");
    end when;
    
  annotation(
      Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)),
      __OpenModelica_simulationFlags(jacobian = "coloredNumerical", s = "dassl", lv = "LOG_STATS"),
      uses(Modelica(version = "3.2.2")),
      Icon(graphics = {Text(origin = {4, -1}, extent = {{-62, 73}, {62, -73}}, textString = "Input
Interface", fontName = "DejaVu Sans Mono Bold"), Rectangle(origin = {-1, -8}, extent = {{-99, 68}, {101, -52}})}, coordinateSystem(initialScale = 0.1)));

end InputInterfaceBlock;














  block OutputInterfaceBlock
    parameter Real dt_sample = 0.1;
  
    CPSModel.ConnectionObjects.Socket con = CPSModel.ConnectionObjects.Socket("_projectDir_/rpcSocket");
    
    Modelica.Blocks.Interfaces.RealInput u annotation(
      Placement(visible = true, transformation(origin = {2, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  
  equation
    when sample(0,dt_sample) then
      CPSModel.Functions.set(con, "y", u);
    end when;  
     
    annotation(
      __OpenModelica_simulationFlags(jacobian = "coloredNumerical", s = "dassl", lv = "LOG_STATS"),
      uses(Modelica(version = "3.2.2")),
      Icon(graphics = {Text(origin = {4, -1}, extent = {{-62, 73}, {62, -73}}, textString = "Output
Interface"), Rectangle(origin = {0, -11}, extent = {{-100, 51}, {100, -29}})}, coordinateSystem(initialScale = 0.1)),
      Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  end OutputInterfaceBlock;



  block OutputArrayBlock
    parameter Real dt_sample = 0.1;
    parameter Integer len;
    parameter String variableName;
  
    CPSModel.ConnectionObjects.Socket con = CPSModel.ConnectionObjects.Socket("_projectDir_/rpcSocket");
    
    Modelica.Blocks.Interfaces.RealVectorInput[len] u annotation(
      Placement(visible = true, transformation(origin = {2, 12}, extent = {{-10, -10}, {10, 10}}, rotation = 0), iconTransformation(origin = {-100, 0}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  
  equation
    when sample(0,dt_sample) then
      CPSModel.Functions.setArray(con, variableName, u, len);
    end when;  
     
    annotation(
      Icon(graphics = {Text(origin = {4, -1}, extent = {{-62, 73}, {62, -73}}, textString = "Output
Array"), Rectangle(origin = {0, -11}, extent = {{-100, 51}, {100, -29}})}, coordinateSystem(initialScale = 0.1)),
      Placement(visible = true, transformation(origin = {-70, 70}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  
  end OutputArrayBlock;

















  model MechatronicSystem
  CPSModel.Models.InputInterfaceBlock inputInterfaceBlock1(variableName = "x")  annotation(
      Placement(visible = true, transformation(origin = {-68, 50}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Math.Product product1 annotation(
      Placement(visible = true, transformation(origin = {-14, 26}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  Modelica.Blocks.Sources.Cosine cosine1(freqHz = 1)  annotation(
      Placement(visible = true, transformation(origin = {-72, 14}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  CPSModel.Models.OutputArrayBlock outputArrayBlock1(len = 2, variableName = "ty")  annotation(
      Placement(visible = true, transformation(origin = {30, 30}, extent = {{-10, -10}, {10, 10}}, rotation = 0)));
  equation
    outputArrayBlock1.u[1] = time;
    connect(product1.y, outputArrayBlock1.u[2]) annotation(
      Line(points = {{-2, 26}, {8, 26}, {8, 30}, {20, 30}, {20, 30}}, color = {0, 0, 127}));
    connect(product1.u2, cosine1.y) annotation(
      Line(points = {{-26, 20}, {-50, 20}, {-50, 14}, {-60, 14}, {-60, 14}}, color = {0, 0, 127}));
    connect(product1.u1, inputInterfaceBlock1.y) annotation(
      Line(points = {{-26, 32}, {-50, 32}, {-50, 50}, {-58, 50}, {-58, 50}, {-58, 50}, {-58, 50}}, color = {0, 0, 127}));
    annotation(
      uses(Modelica(version = "3.2.2")));
  end MechatronicSystem;




end Models;
